/* 
 * The MIT License
 *
 * Copyright 2017 Laith Zaki (pseudonym 7Hazard).
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

var answered = false;
function setAnswered(bool){
    answered = bool;
}

var correct; // Decleration of correct answer for Quest
function setCorrect(correctanswer){
    correct = correctanswer;
}

function showResult(answer){
    if (!answered){
        // Check wether the correct answer was given
        if (answer === correct){
            $("#"+answer.split(" ")).addClass("green darken-2"); // Make the answer green
            $("#nquest").html("Next Question ->"); // Change from skip to next question
        } else {
            $("#"+answer.split(" ")).addClass("red darken-4"); // Make answer red
            $("#"+correct.split(" ")).addClass("green darken-2"); // Make actual correct answer green
            $("#nquest").html("Next Question ->"); // Change from skip to next question
        }
        answered = true;
    }
}